package Tugas1PBOiqbalhh;

import java.util.Scanner;

public class Kelilinglingkaran {
    public static void main(String[] args) {

       //Inisialisasi objek input dari class scanner
       Scanner input=new Scanner(System.in);

       //Deklarasi variabel
       double kelilinglingkaran, phi=3.14;
       int d;

       //Input nilai Diameter
       System.out.print("Masukan nilai Diameter : ");
       d=input.nextInt();

       //Menghitung keliling lingkaran
       kelilinglingkaran=phi*d;

       //Tampilkan hasil
       System.out.println("Keliling lingkaran = "+kelilinglingkaran);
    } 
}


//Iqbalhasanuhamdani-A11.2020.12805
//Keliling lingkaran dengan diketahui diameter = 10. 
//Masukan Diameter 10 .