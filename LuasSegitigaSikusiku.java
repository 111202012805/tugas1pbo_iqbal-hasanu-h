package Tugas1PBOiqbalhh;

import java.util.*;

public class LuasSegitigaSikusiku {

	public static void main(String[] args)	{
		double L, a, t;
		
		//Inisialisasi objek input dari class scanner
		Scanner input =new Scanner(System.in);
		
		//Input nilai Alas
		System.out.print("Masukan Alas : ");
		a = input.nextDouble();
		
		 //Input nilai Tinggi
		
		System.out.print("Masukan Tinggi : ");
		t = input.nextDouble();
		
		//Menghitung Luas segitiga siku-siku
		L = 0.5 * a * t;
		
		System.out.println("Luas segitiga siku-siku : "+L);  
		
		
	}
}

//Iqbalhasanuhamdani-A11.2020.12805
//Luas segitiga siku-siku dengan diketahui alas = 6, tinggi = 8
